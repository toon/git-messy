DIR ?= tmp

default: all

clean:
	rm -rf $(DIR)

$(DIR)/:
	mkdir -p $@
	cd $(DIR) && git init
	cd $(DIR) && git config user.email 'dummy@example.com'

TARGETS += $(DIR)/.gitignore
$(DIR)/.gitignore: $(DIR)/
	cp .gitignore $@

fsck:
	cd $(DIR) && git fsck --full

all: $(TARGETS)

.PHONY: default all clean init

# GIT-FSCK(1)
#
# EXTRACTED DIAGNOSTICS
#        expect dangling commits - potential heads - due to lack of head information
#            You haven't specified any nodes as heads so it won't be possible to differentiate between un-parented commits and
#            root nodes.
#
#        missing sha1 directory <dir>
#            The directory holding the sha1 objects is missing.
#
#        unreachable <type> <object>
#            The <type> object <object>, isn't actually referred to directly or indirectly in any of the trees or commits seen.
#            This can mean that there's another root node that you're not specifying or that the tree is corrupt. If you haven't
#            missed a root node then you might as well delete unreachable nodes since they can't be used.
#
#        missing <type> <object>
#            The <type> object <object>, is referred to but isn't present in the database.
#
#        dangling <type> <object>
#            The <type> object <object>, is present in the database but never directly used. A dangling commit could be a root
#            node.
#
#        sha1 mismatch <object>
#            The database has an object who's sha1 doesn't match the database value. This indicates a serious data integrity
#            problem.
#
